Base
====

Basic role to setup the most common stuff. It will install debugging tools like `htop` and setup a nice `/etc/motd`.

Requirements
------------

This role assumes a minimal CentOS 8 or Archlinux installation and will install all necessary dependencies.

Role Variables
--------------

### Role defaults

| Variable                    | Type        | Default  | Description |
| --------------------------- | ----------- | -------- | ----------- |
| `base__`           | ``       | `` |  |

### Role variables

| Variable           | Type        | Description |
| ------------------ | ----------- | ----------- |
| `base__` | `` |  |

### Role parameters

| Variable                    | Type  | Description |
| --------------------------- | ----- | ----------- |
| `base__` | `` |  |

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: xengi.base
```

License
-------
MIT

Author Information
------------------

- Ricardo (XenGi) Band <email@ricardo.band>
